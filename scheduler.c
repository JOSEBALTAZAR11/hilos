#include "scheduler.h"
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <time.h>

#define init_time_task  0
int option, num_tasks=0;
pthread_mutex_t llave;


int main(void)
{
	
	
	
	do{
		
		printf("1.-Nueva tarea\n");
		printf("2.-Listar tareas\n");
		printf("3.-Iniciar tareas\n");
		printf("5.- Salir\n");
		printf("Ingresa la funcion a realizar: ");
		scanf("%d", &option);
		switch (option)
		{
			case 1:
			{
				printf("Cuantos 'Tasks' agregara a la cola?:");
				scanf("%d", &num_tasks);				
				UploadDataTotask(num_tasks);					
				break;
			}
			case 2:
			{
				for (int k = 0; k < num_tasks; k++)
				{
					if (task[k].time_execute > 0)
					{
						
						printf("\n");
						printf("tarea[%i]\n", k);
						printf("|tarea ID: %d\n",task[k].id);
						printf("|nombre de la tarea: %s\n",task[k].name_task);
						printf("|prioridad de la tarea: %d\n",task[k].priority);
						printf("|tiempo de ejecucion %d\n",task[k].time_execute);
					}
				}
				break;
			}
			case 3:
			{
				char flag[20] = "FINALIZADO";
				for (int i = 0; i < num_tasks; i++)
				{
					if (task[i].time_execute==0)
					{
						printf("NO HAY PROCESOS EN COLA DE ESPERA\n");
						break;
					}else
					{


						printf("ORDENANDO tareas POR PRIORIDAD\n");
						SortArray(num_tasks);
						for (int i = 0; i < 50; i++)
						{
							delay(3);
						}
						printf("OK\n");
						for (int i = 0; i < 50; i++)
						{
							delay(2);
						}
						system("clear");
						printf("IMPRIMIENDO PRIORIDAD ORDENADA\n");

						for (int i = 0; i < num_tasks; i++)
						{
							printf("\n");
							printf("tarea[%i]\n", i);
							printf("|tarea ID: %d\n",task[i].id);
							printf("|nombre de la tarea: %s\n",task[i].name_task);
							printf("|prioridad de la tarea: %d\n",task[i].priority);
							printf("|tiempo de ejecucion %d\n",task[i].time_execute);
							
						}
						for (int i = 0; i < 50; i++)
						{
							delay(3);
						}
						printf("INICIALIZANDO PROCESOS\n");
						for (int i = 0; i < 50; i++)
						{
							delay(3);
						}
						printf("POR FAVOR ESPERE\n");
						for (int i = 0; i < 50; i++)
						{
							delay(3);
						}
						printf("OK\n");
						system("clear");
				        pthread_t hilo1;
						pthread_mutex_init(&llave,NULL);
						pthread_create(&hilo1,NULL,Ejecutartareas,NULL);
						pthread_join(hilo1,NULL);
						printf("PROCESOS FINALIZADOS: \n");
							for (int i = 0; i < 50; i++)
								{
									delay(3);
								}
							for (int i = 0; i < num_tasks; i++)
							{
								printf("\n");
								printf("tarea[%i]\n", i);
								printf("|tarea ID: %d\n",task[i].id);
								printf("|nombre de la tarea: %s\n",task[i].name_task);
								printf("|prioridad de la tarea: %d\n",task[i].priority);
								printf("|tiempo de ejecucion %d\n",task[i].time_execute);
							}
					}
				}
				break;
			}
			case 4:
			{
				
				printf("OK\n");
				for (int i = 0; i < 50; i++)
				{
					delay(3);
				}
				num_tasks = 0;
				system("clear");	
				break;
			}
			case 5:
			{
				printf("FINALIZANDO.");
				for (int i = 0; i < 50; i++)
				{
					delay(5);
					printf(".\n");	
				}
				system("clear");
				break;
			}
			default :
			{
				printf("OPCION INGRESADA INVALIDA\n");
				for (int i = 0; i < 50; i++)
				{
					delay(5);
				}
				system("clear");
				break;
			}
		}
	}while(option != 5);	
	return 0;
}

void *Ejecutartareas(void *args){
	int i,x=1,y=5;
	printf("espere para bloquear\n\n");
	pthread_mutex_lock(&llave);//banderin para hacer esperar a los hilos
	printf("key bloqueada\n\n");
	printf("ejecutando\n");
	for (int i = 0; i < 50; i++){
		delay(4);
	}                            	                          
	for(i = 0; i<num_tasks; i++){
		fflush(stdout);
		printf( "\nproceso[%s", task[i].name_task);
		printf( "\nId: [%d", task[i].id);
		printf( "\nprioridad: [%d", task[i].priority); 
		do{      
			printf( "\n%d seg", task[i].time_execute);
			task[i].time_execute = task[i].time_execute - 1;
		    for (int i = 0; i < 50; i++){
				delay(3);
			}
		} while (task[i].time_execute != 0);
	}
	system("clear");
	printf("\nterminado\n");
	for(int i = 0; i < num_tasks; i++){      
		printf( "\nid: %d", task[i].id);
		printf( "\nProceso: %s", task[i].name_task);
		printf( "\nprioridad: %d", task[i].priority);
		printf( "\tiempo de ejecucion: %d \n", task[i].time_execute);
	} 
	printf("\n");
	pthread_mutex_unlock(&llave);
	printf("*key bloqueada\n");
	return NULL;
} 


void delay(int num_of_seconds)
{
	int miliseconds = 10000 * num_of_seconds;
	clock_t start_time = clock();
	while(clock() < start_time + miliseconds);
}

void UploadDataTotask(int num_tasks)
{
	for (int i = 0; i < num_tasks; i++)
	{
		printf("T A S K [%d]\n", i);
		printf("Ingresa el id del task: ");
		scanf("%i", &task[i].id);
		printf("Ingresa el nombre del task: ");
		scanf("%s", task[i].name_task);
		printf("Ingresa la prioridad del task: ");
		scanf("%d", &task[i].priority);
		printf("Ingresa el tiempo que deseas ejecutar el task: (seg)");
		scanf("%d", &task[i].time_execute);
	}
}

void SortArray(int num_tasks){
	int temporal, temporal2, temporal3;
	char temporal4[50];

	for (int i = 0;i < num_tasks; i++){
		for (int j = 0; j< num_tasks-1; j++){
			if (task[j].priority < task[j+1].priority){
			temporal = task[j].priority; 
			temporal2= task[j].id;
			temporal3=task[j].time_execute;
			strcpy(temporal4,task[j].name_task);
			task[j].priority = task[j+1].priority; 
			task[j].id = task[j+1].id;
			task[j].time_execute=task[j+1].time_execute;
			strcpy(task[j].name_task,task[j+1].name_task);
			task[j+1].priority = temporal;
			task[j+1].id=temporal2;
			task[j+1].time_execute=temporal3;
			strcpy(task[j+1].name_task,temporal4);
			}
		}
	}
}